---

- name: "Include package dependencies for building R on Ubuntu {{ ansible_distribution_major_version | int }}"
  ansible.builtin.include_vars: "dependencies-build-{{ ansible_distribution_major_version | int }}.yml"
  when: ansible_distribution == "Ubuntu"

- name: Set R unpack directory
  ansible.builtin.set_fact:
    R_unpack_dir: "{{ ansible_env.HOME }}/.cache"

## COMPILE R FROM SOURCE

- name: Install packages required for compiling R
  ansible.builtin.apt:
    state: present
    install_recommends: no
    name: "{{ R_build_dependencies }}"

# Even if install_R was requested, only install R if
# the requested R version is not already installed
# (this check is meant to avoid time-consuming unnecessary re-installs)
- name: "Check if R v{{ R_version }} is already installed"
  ansible.builtin.stat:
    path: "{{ R_directory }}/{{ R_version }}/bin/R"
  register: R_latest_installed


- name: Compile R
  when: not (R_latest_installed.stat.exists | bool)
  block:

    - name: "Download and inflate R v{{ R_version }} tarball into {{ R_unpack_dir }}"
      ansible.builtin.unarchive:
        remote_src: yes
        src: "https://cran.r-project.org/src/base/R-{{ R_version | first }}/R-{{ R_version }}.tar.gz"
        dest: "{{ R_unpack_dir }}"
        owner: "{{ ansible_env.USER }}"
        group: "{{ ansible_env.USER }}"

    # texi2any and friends are installed in /usr/bin by the texinfo system package
    # Although it seems setting this explicitly has no effect
    - name: "Set TEXI2ANY=/usr/bin/texi2any in config.site"
      ansible.builtin.lineinfile:
        dest: "{{ R_unpack_dir }}/R-{{ R_version }}/config.site"
        regexp: "^## TEXI2ANY"
        line: "TEXI2ANY=/usr/bin/texi2any"
        state: present

    # Note: during configure, R checks for installed TeX, pdfTeX and friends.
    # This is why this role depends on the texlive role.
    - name: Compile R | Configure
      ansible.builtin.shell: >
        . /etc/profile &&
        ./configure --prefix={{ R_directory }}/{{ R_version }}
        --enable-R-shlib --with-blas --with-lapack --with-tcltk
      args:
        chdir: "{{ R_unpack_dir }}/R-{{ R_version }}/"

    - name: Compile R | Make
      ansible.builtin.shell: ". /etc/profile && make"
      args:
        chdir: "{{ R_unpack_dir }}/R-{{ R_version }}/"

    - name: Compile R | Make install
      ansible.builtin.shell: ". /etc/profile && make install"
      args:
        chdir: "{{ R_unpack_dir }}/R-{{ R_version }}/"
  # END OF BLOCK
