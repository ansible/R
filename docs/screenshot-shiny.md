# Taking screenshot of Shiny apps

In lieu of a good way to convert [wordclouds](https://github.com/Lchiffon/wordcloud2)
via knitr/tikzDevice to LaTeX, I have used [webshot](https://github.com/wch/webshot)
to "screenshot" the HTML output of wordcloud2 and convert it to PDF with high fidelity.

`webshot` requires the external program [PhantomJS](https://github.com/ariya/phantomjs),
but the latter is no longer in development (since at least 2018).


## Alternatives to webshot/phantomjs

### webshot/phantomjs on life support

Set `Sys.setenv(OPENSSL_CONF="/dev/null")`.
[This should allow `phantomjs` to work](https://github.com/wch/webshot/issues/115#issuecomment-1505780161).

If possible I would like to avoid this hack.


### shinyscreenshot

> It allows you to capture screenshots of entire pages or parts of pages in Shiny apps,
> and have the image downloaded as a PNG automatically. It can be used to capture
> the current state of a Shiny app, including interactive widgets
> (such as plotly, timevis, maps, etc).

Call the `screenshot()` function from inside a Shiny `server` call.
That makes it unsuitable for my use case.

+ https://deanattali.com/blog/shinyscreenshot-release


### snapper

Requires the `canvas2html` JavaScript library. Not on CRAN.
18 stars on Github.

+ https://github.com/yonicd/snapper


### capture

Uses [html-to-image](https://github.com/bubkoo/html-to-image) to convert
to PNG and [jsPDF](https://github.com/parallax/jsPDF) to convert to PDF.
Similar to `shinyscreenshot` meant to be called from inside shiny `server`.

+ https://github.com/dreamRs/capture


### webshot2

> `webshot2` is meant to be a replacement for `webshot`, except that instead
> of using `PhantomJS`, it uses headless Chrome via the `Chromote` package.

This means adding `webshot2` and `chromote` to `R_CRAN_packages`,
and adding the `browser-chromium` role as a dependency of this role.

+ https://github.com/rstudio/chromote



## Links and notes

+ https://github.com/Lchiffon/wordcloud2
+ https://github.com/wch/webshot
+ https://github.com/ariya/phantomjs
+ https://towardsdatascience.com/create-a-word-cloud-with-r-bde3e7422e8a
