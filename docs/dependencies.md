# Dependencies

> I think we can now finally remove the dependency on PHP
> thanks to our disuse of mounting the full R tree on remote
> workstations.

## Removing dependence on PHP roles while providing up-to-date libicu package

On hosts that access R via ssh (usually workstations),
R fails to start: `error while loading shared libraries: libicuuc.so.65`.
It seems v65 is missing on the workstations, but not on `luxor`,
(because the host got the PHP roles).

A simple demonstration (luxor is the host, harbin is a workstation):
```
taha@luxor:~
$ ldconfig -p | grep icuuc
libicuuc.so.65 (libc6,x86-64) => /usr/lib/x86_64-linux-gnu/libicuuc.so.65
libicuuc.so.60 (libc6,x86-64) => /usr/lib/x86_64-linux-gnu/libicuuc.so.60
libicuuc.so (libc6,x86-64) => /usr/lib/x86_64-linux-gnu/libicuuc.so
taha@harbin:~
$ ldconfig -p | grep icuuc
libicuuc.so.60 (libc6,x86-64) => /usr/lib/x86_64-linux-gnu/libicuuc.so.60
libicuuc.so (libc6,x86-64) => /usr/lib/x86_64-linux-gnu/libicuuc.so
 ```

I would like to provide this dependency package *without* making this role
depend on the PHP roles.

But which package exactly in the PHP role is it that causes installation of `libicuuc.so`?
It's the `icu` package:
```
taha@luxor:~
$ dpkg -l | grep icu
ii  icu-devtools                65.1-1+ubuntu18.04.1+deb.sury.org+1    amd64    Development utilities for International Components for Unicode
ii  libharfbuzz-icu0:amd64      1.7.2-1ubuntu1                         amd64    OpenType text shaping engine ICU backend
ii  libicu-dev:amd64            65.1-1+ubuntu18.04.1+deb.sury.org+1    amd64    Development files for International Components for Unicode
ii  libicu-le-hb-dev:amd64      1.0.3+git161113-4                      amd64    ICU Layout Engine API on top of HarfBuzz shaping library (development)
ii  libicu-le-hb0:amd64         1.0.3+git161113-4                      amd64    ICU Layout Engine API on top of HarfBuzz shaping library
ii  libicu60:amd64              60.2-3ubuntu3.1                        amd64    International Components for Unicode
ii  libicu65:amd64              65.1-1+ubuntu18.04.1+deb.sury.org+1    amd64    International Components for Unicode
ii  libiculx60:amd64            60.2-3ubuntu3.1                        amd64    International Components for Unicode
```

Specifically, `libicu65` is required by the PHP modules `intl` and `http`:
```
$ apt rdepends libicu65
libicu65
Reverse Depends:
  Depends: icu-devtools (>= 65.1-1~)
  Depends: php8.0-intl (>= 65.1-1~)
  Depends: php8.0-http (>= 65.1-1~)
  Depends: php7.4-intl (>= 65.1-1~)
  Depends: php7.4-http (>= 65.1-1~)
  Depends: php7.3-intl (>= 65.1-1~)
  Depends: php7.3-http (>= 65.1-1~)
  Depends: php7.2-intl (>= 65.1-1~)
  Depends: php7.2-http (>= 65.1-1~)
  Depends: php7.1-intl (>= 65.1-1~)
  Depends: php7.1-http (>= 65.1-1~)
  Depends: php7.0-intl (>= 65.1-1~)
  Depends: php7.0-http (>= 65.1-1~)
  Depends: php5.6-intl (>= 65.1-1~)
  Depends: libxml2 (>= 65.1-1~)
  Depends: libicu-dev (= 65.1-1+ubuntu18.04.1+deb.sury.org+1)
```

Since we already have this package installed (managed via apt from a PPA)
on luxor, I think the easiest solution is to repack it onto a DEB-file,
which we simply include in this role.

```
taha@luxor:/media/bay/taha/projects/ansible
$ apt-cache search libicu
libicu-le-hb-dev - ICU Layout Engine API on top of HarfBuzz shaping library (development)
libicu-le-hb0 - ICU Layout Engine API on top of HarfBuzz shaping library
libiculx60 - International Components for Unicode
icu-devtools - Development utilities for International Components for Unicode
libicu-dev - Development files for International Components for Unicode
libicu60 - International Components for Unicode
libicu4j-4.2-java - Library for Unicode support and internationalization
libicu4j-4.4-java - Library for Unicode support and internationalization
libicu4j-49-java - Library for Unicode support and internationalization
libicu4j-java - Library for Unicode support and internationalization
libicu4j-java-doc - Library for Unicode support and internationalization - Docs
libicu64 - International Components for Unicode
libicu65 - International Components for Unicode
```

Note libicu65 above. Which is conspicuously absent on a workstation:
```
taha@mkem150:/media/bay/taha/projects/ansible
(main) $ apt-cache search libicu
icu-devtools - Development utilities for International Components for Unicode
libicu-dev - Development files for International Components for Unicode
libicu-le-hb-dev - ICU Layout Engine API on top of HarfBuzz shaping library (development)
libicu-le-hb0 - ICU Layout Engine API on top of HarfBuzz shaping library
libicu60 - International Components for Unicode
libiculx60 - International Components for Unicode
libicu4j-4.2-java - Library for Unicode support and internationalization
libicu4j-4.4-java - Library for Unicode support and internationalization
libicu4j-49-java - Library for Unicode support and internationalization
libicu4j-java - Library for Unicode support and internationalization
libicu4j-java-doc - Library for Unicode support and internationalization - Docs
```

`dpkg-repack` is the name of the game! On the machine with PHP installed:

```
taha@luxor:~
$ apt-cache search dpkg-repack
dpkg-repack - Debian package archiving tool
$ sudo apt install dpkg-repack
$ dpkg-repack libicu65
taha@luxor:~/Downloads/icu
$ dpkg-repack libicu65
dpkg-deb: building package 'libicu65' in './libicu65_65.1-1+ubuntu18.04.1+deb.sury.org+1_amd64.deb'.
taha@luxor:~/Downloads/icu
$ ll
total 8.1M
drwxrwxr-x 2 taha taha 4.0K Jun  9 21:41 .
drwxrwxr-x 5 taha taha 4.0K Jun  9 21:41 ..
-rw-r--r-- 1 taha taha 8.1M Jun  9 21:41 libicu65_65.1-1+ubuntu18.04.1+deb.sury.org+1_amd64.deb
```
Fantastic!
For completeness, note which packages that `libicu65` depends on:
```
$ apt depends libicu65
libicu65
  Depends: libc6 (>= 2.14)
  Depends: libgcc1 (>= 1:3.0)
  Depends: libstdc++6 (>= 5.2)
  Breaks: <libiculx63> (<< 63.1-5)
  Breaks: openttd (<< 1.8.0-2~)
  Replaces: <libiculx63> (<< 63.1-5)
```

That worked nicely.
I considered a few other approaches (recorded for posterity):

pkgs.org lists an [RPM package](https://pkgs.org/download/libicu65) for `libicu65`.
That site also has DEB packages, but I cannot find v65 (there is v60, v66, others).

The [ICU project's own website for v65](http://site.icu-project.org/download/65).
That release even has its [own Github repo](https://github.com/unicode-org/icu/tree/maint/maint-65). Actually, it's even better: all previous releases have precompiled tarballs
([here's v65](https://github.com/unicode-org/icu/releases/download/release-65-1/icu4c-65_1-Ubuntu18.04-x64.tgz)).
This tarball contains precompiled binaries, it's just a matter of extracting them
into their proper places in the filesystem.

And [here's instructions on how to build it from source](https://unicode-org.github.io/icu/userguide/icu4c/build#how-to-build-and-install-on-unix)


+ https://ostechnix.com/recreate-debian-binary-packages-that-are-already-installed-on-a-system/
+ https://bbs.archlinux.org/viewtopic.php?id=220224
+ https://packages.ubuntu.com/search?keywords=libicu
