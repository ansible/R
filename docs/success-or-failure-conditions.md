# Note on success/failure conditions when installing R packages

For example, we install CRAN packages with this task:

```
- name: Install R CRAN packages
  ansible.builtin.command: >
    xvfb-run --auto-servernum {{ R_bin_path }}/Rscript --slave --no-save --no-restore-history -e
    "Sys.setenv(RPYTHON_PYTHON_VERSION=3);
    if (! ('{{ item }}' %in% installed.packages()[, 'Package'])) {
      install.packages(pkgs='{{ item }}', lib='{{ RLIBS_SYSTEM }}');
      print('Added {{ item }}');
    } else {
      print('Already installed {{ item }}');
    }"
  register: r_cran_package
  failed_when: >
    r_cran_package.rc != 0 or
    'had non-zero exit status' in r_cran_package.stderr or
    'had non-zero exit status' in r_cran_package.stdout
  changed_when: "'Added' in r_cran_package.stdout"
  loop: "{{ R_CRAN_packages }}"
```

The `failed_when` is how we attempt to pickup on the signals R emits
(either in the stdout console or in stderr) on whether the package installation
succeeded or not.

This logic used to consist of only
`r_cran_package.rc != 0 or 'had non-zero exit status' in r_cran_package.stderr`,
but during upgrade of my main R server from Bionic to Jammy (and subsequent
reinstallation of R packages) I noticed a failed package installation that was
not picked up as failed by our Ansible task:

```
2022-07-27 01:02:35,450 p=141585 u=taha n=ansible | changed: [luxor] => (item=nloptr) => changed=true 
  ansible_loop_var: item
  cmd:
  - xvfb-run
  - --auto-servernum
  - /opt/R/4.1.3/bin/Rscript
  - --slave
  - --no-save
  - --no-restore-history
  - -e
  - |-
    Sys.setenv(RPYTHON_PYTHON_VERSION=3);  if (! ('nloptr' %in% installed.packages()[, 'Package'])) {
      install.packages(pkgs='nloptr', lib='/opt/R/4.1.3/lib/R/library');
      print('Added nloptr');
    } else {
      print('Already installed nloptr');
    }
  delta: '0:00:14.827362'
  end: '2022-07-27 01:02:35.419858'
  failed_when_result: false
  item: nloptr
  msg: ''
  rc: 0
  start: '2022-07-27 01:02:20.592496'
  stderr: ''
  stderr_lines: <omitted>
  stdout: |-
    trying URL 'https://www.stats.bris.ac.uk/R/src/contrib/nloptr_2.0.3.tar.gz'
    Content type 'application/x-gzip' length 2219877 bytes (2.1 MB)
    ==================================================
    downloaded 2.1 MB
  
    * installing *source* package ‘nloptr’ ...
    [...]
    checking for cmake... no
    CMake was not found on the PATH. Please install CMake
  
    ERROR: configuration failed for package ‘nloptr’
    * removing ‘/opt/R/4.1.3/lib/R/library/nloptr’
  
    The downloaded source packages are in
            ‘/tmp/RtmpBwh0Kr/downloaded_packages’
    Updating HTML index of packages in '.Library'
    Making 'packages.html' ... done
    [1] "Added nloptr"
    Warning message:
    In install.packages(pkgs = "nloptr", lib = "/opt/R/4.1.3/lib/R/library") :
      installation of package ‘nloptr’ had non-zero exit status
  stdout_lines: <omitted>
```

Either the message `had non-zero exit status` used to be printed to `stderr`
for earlier R versions or Ansible versions (or else for Bionic) but is now 
printed to `stdout`, or else this logic was flawed from the start.

Simply adding a check for the same string in `stdout` should make the conditional
able to detect package installation failures such as the above.
